# HTML

# 웹페이지의 단 구조

## 가로 정렬

### clearfix

플로팅 박스가 뒤에 있는 박스의 레이아웃에 영향을 주지 않게 만드는 기술.



#### HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="box1">
    BOX1
</div>

<div class="boxA">
    <div class="box2">
        BOX2
    </div>
    <div class="box3">
        BOX3
    </div>
</div>

<div class="box4">
    BOX4
</div>

</body>
</html>
```

#### CSS

```css
/* clearfix */

/* .boxA {
    border: solid 5px #00a0e9;
    background-color: #a0e0fe;
    color: #00a0e9;
} */

.boxA:after {
    content: "";
    display: block;    /* box4가 box2,3를 무시하는 것을 막기 위해
                        * :after의 clear 속석을 both로 지정
                        * 글자가 오른쪽에 붙지 않고 플로팅박스 아래에
                        * 출력*/
    clear: both;
}

.box1 {
    border: solid 2px #be00e9;
    background-color: #fbb3fe;
    color: #e90b58;
}


.box2 {
    border: solid 2px #e95c5a;
    background-color: #fee2e0;
    color: #e93d69;
    width: 50px;
    height: 50px;

    float: left;     
    /* floating box가 됨. 다른 박스는 floating box의 존재를 인식 못함 */
}

.box3 {
    border: solid 2px #2be993;
    background-color: #c4fecf;
    color: #3fe900;
    width: 50px;
    height: 50px;

    float: left;      
    /* floating box가 됨. 다른 박스는 floating box의 존재를 인식 못함 */
}

.box4 {
    border: solid 2px #e99f02;
    background-color: #fee89b;
    color: #ff6817;
}

```





## 박스 분할

> '박스를 분할하는 것'과 '박스를 가로로 정렬하는 것'은 항상 함께 움직인다. 둘을 모두 완벽하게 적용할 수 있게 제대로 기억하기.



### 박스를 가로로 분할

```html
<div class="boxA">
    <div class="box1">BOX1</div>
    <div class="box2">BOX2</div>
</div>
```

```css
.boxA:after {content: "";
			display: block;
			clear: both;}

.box1 {float: left;
		width: 50%;}

.box2 {float: left;
		width: 50%;}
```



### 칼럼 탈락

```css
.boxA:after { content: "";
	display: block;
	clear: both;
}

.box1 { float: left;
	width: 50%;
}

.box2 { float: left;
	width: 50%;
}

.box3 { float: left;
	width: 100%;
}
```



#### 칼럼 탈락 사용시 주의사항

위 코드처럼 box3를 개행해서 출력하고 싶다면 가로로 늘어선 박스의 높이가 같아야 한다.

* 해결방법

```css
.box3 { float: left;
	width: 100%;
}

.box3 { float: left;
	width: 50%;
	clear: both;
}
```







## 박스 너비 고정

#### HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/div_fixed_size.css">
</head>
<body>
    <div class="box1">BOX1</div>
    <div class="boxA">
        <div class="box2">
            <p>test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test 
            </p>
        </div>
       
        <div class="box3">BOX3</div>
    </div>
    <div class="box4">BOX4</div>
</body>
</html>
```

#### CSS

```css
.box1 {
    border: solid 3px salmon;
    background-color: bisque;
}

.boxA {
    border: solid 3px blue;
}

.boxA:after {
    content: "";
    display: block;
    clear: both;
}

.box2 {
    border: solid 3px green;
    
    float: left;
    width: 100%;
    margin-right: -312px;
    padding-right: 312px;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: : border-box;
    box-sizing: border-box;    
    /* border-box : 여백이 width 속성으로 지정한 너비에 포함되어 처리*/
}

.box3 {
    border: solid 3px yellowgreen;
    
    float: left;
    width: 300px;
}

.box4 {
    border: solid 3px red;
}
```





## 박스 정렬 순서 지정

```html
<body>
    <div class="boxA">        
        <div class="box1"></div>
        <div class="box2"></div>
        <div class="box3"></div>
        <div class="box4"></div>
    </div>
</body>
```

```css
.boxA {
    border: solid 3px blue;
}

.boxA:after {
    content: "";
    display: block;
    clear: both;
}

.box1 {
    border: solid 1px salmon;

    float: left;
    width: 24%;
}

.box2 {
    border: solid 1px green;
    
    float: left;
    width: 24%;
}

.box3 {
    border: solid 1px yellowgreen;
    
    float: left;
    width: 24%;
}

.box4 {
    border: solid 1px red;

    float: left;
    width: 24%;
}
```

`float` 을 지정하는 것에 따라 순서가 바뀐다.

 `left` , `right` , `left` , `right` 로 지정하면 왼쪽과 오른쪽에 번갈아가며 붙는다.

===> `box1` `box3` `box4` `box2`







## 반응형 웹 디자인

### 반응형 웹 디자인의 기본

박스의 가로 정렬을 해제하면 박스는 세로 정렬 상태로 돌어간다. (세로 정렬이 기본)

브라우저 화면의 너비에 따라 가로 정렬을 해제하려면 가로 정렬과 관련된 설정을 `@media() {~}` (미디어 쿼리) 로 감싸고 해제할 너비를 지정한다.

```css
@media (min-width: 768) {
    .boxA:after {...;}
    .box1 {...;}
    .box2 {...;}
}
```



### 반응형 웹 디자인 응용

```html
<body>
    <div class="box1">BOX1</div>
    
    <div class="boxA">
        <div class="boxB">
            <div class="box2">BOX2</div>
            <div class="box3">BOX3</div>
        </div>
        
        <div class="box4">BOX4</div>
    </div>
</body>
```

```css

.box1{
    border: solid 3px coral;
    background-color: rgb(233, 187, 170);
    width:100%;
    height: 100px;
}

.boxA{
    
}


@media (min-width: 768px) {
.boxA:after {
    content: "";
    display: block;
    clear: both;
}

.boxB {
    border: solid 3px green;
    float: left;
    width: 80%;
}

.boxB:after {
    content: "";
    display: block;
    clear: both;
}

.box2{
    float: right;
    width: 75%;
    height: 500px;
}


.box3{   
    float: right;
    width: 24%;
    height: 500px;
}

}

.box2 {
    border: solid 2px blue;
    background-color: rgb(187, 187, 248);    
}

.box3 {
    border: solid 2px red;
    background-color: rgb(236, 133, 133);
 
}

.box4{
    border: solid 2px green;
    background-color: rgb(215, 238, 182);
    
    height: 500px;
    float: left;
    width: 18%;

}
```





### 중단단계 추가

